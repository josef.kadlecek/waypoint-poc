# Getting Started with Waypoint

1. Run `waypoint init` in this example directory with `waypoint.hcl`.
1. Run `waypoint up` to build and deploy the application.
