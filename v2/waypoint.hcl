project = "nomad-jobspec-nodejs"

app "nodejs-jobspec-web" {
  build {
    use "pack" {}
    registry {
      use "docker" {
        image = "10.0.228.190:5000/nodejs-jobspec-web"
        tag   = "1"
        local = false
      }
    }
  }

  deploy {
    use "nomad-jobspec" {
      // Templated to perhaps bring in the artifact from a previous
      // build/registry, entrypoint env vars, etc.
      jobspec = templatefile("${path.app}/app.nomad.tpl")
    }
  }

  release {
    use "nomad-jobspec-canary" {
      groups = [
        "app"
      ]
      fail_deployment = false
    }
  }
}
