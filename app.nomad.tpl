

job "waypoint-poc" {
  datacenters = ["inuits-play"]

  group "waypoint-poc" {
    #update {
    #  max_parallel = 1
    #  canary       = 1
    #  auto_revert  = false
    #  auto_promote = false
    #  health_check = "task_states"
    #}
    # Example
    #update {
    #  max_parallel = 1
    #  canary       = 1
    #  auto_revert  = true
    #  auto_promote = false
    #  health_check = "task_states"
    #}

#Docs
  update {
    max_parallel      = 3
    health_check      = "checks"
    min_healthy_time  = "10s"
    healthy_deadline  = "5m"
    progress_deadline = "10m"
    auto_revert       = true
    auto_promote      = true
    canary            = 1
    stagger           = "30s"
  }

    network {
      port "http" {
        to = 80
      }
    }

    service {
      port = "http"
      check {
        type     = "http"
        path     = "/"
        interval = "10s"
        timeout  = "5s"
      }
    }

    task "waypoint-poc" {
      driver = "docker"
      config {
        image = "${artifact.image}:${artifact.tag}"
        ports = ["http"]
      }

      env {
        %{ for k,v in entrypoint.env ~}
        ${k} = "${v}"
        %{ endfor ~}

        // For URL service
        PORT = 80
      }

        template {
              data = <<EOF
          # Read single key from Consul KV.
          APP_NAME = "{{key "app/waypoint-poc"}}"

          # Read all keys in the path `app/environment` from Consul KV.
          {{range ls "app"}}
          {{.Key}}={{.Value}}
          {{end}}
              EOF

              destination = "local/env"
              env         = true
        }
    }
  }
}
