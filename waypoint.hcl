project = "waypoint-poc"

variable "registry_password" {
  type    = string
  default = dynamic("vault", {  
    path = "inuits/data/nomad-play/nexus" # SECRET_ENGINE_NAME/PROPERTY_OF_SECRET_ENGINE/pathInEngine --- PROPERTY_OF_SECRET_ENGINE in this case will usually be data
    key = "/data/password"  # In kv v2 we prefix with /data more https://developer.hashicorp.com/waypoint/plugins/vault
  })
}

variable "nexus_user" {
  type    = string
    default = dynamic("vault", {
    path = "inuits/data/nomad-play/nexus"
    key = "/data/username"
  })
}


app "waypoint-poc" {

  build {
    use "docker" {}
    registry {
        use "docker" {
          image = "10.0.228.190:5000/${var.nexus_user}inuitsplay-waypoint-poc"
          insecure = true
          tag = gitrefpretty()
          local = false
        }
    }
  }

  deploy {
    use "nomad-jobspec" {
      jobspec = templatefile("${path.app}/app.nomad.tpl")
    }
  }

  release {
    use "nomad-jobspec-canary" {
      groups = [
        "waypoint-poc"
      ]
      fail_deployment = false
    }
  }

}



